import { products } from "@/helpers/moks";

export function getProducts() {
  if (localStorage.getItem("products")) {
    return JSON.parse(localStorage.getItem("products"));
  } else {
    return products;
  }
}
export function setProducts(products) {
  localStorage.setItem("products", JSON.stringify(products));
}
